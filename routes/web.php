<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/form', [\App\Http\Controllers\AppController::class , 'form'])->name('form');
Route::get('/employees', [\App\Http\Controllers\EmployeeController::class , 'index'])->name('employees');
Route::get('/', [\App\Http\Controllers\AppController::class , 'counter'])->name('home');
