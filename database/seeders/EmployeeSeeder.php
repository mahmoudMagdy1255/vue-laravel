<?php

namespace Database\Seeders;

use App\Models\Employee;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Container::getInstance()->make(Generator::class);

        for ($i = 0; $i < 100; $i++) {

            $employee = Employee::create([
                'name' => $faker->name(),
                'email' => $faker->unique()->safeEmail(),
                'phone' => $faker->phoneNumber(),
            ]);

            $employee->departments()->attach([random_int(1, 5)]);
        }

    }
}
