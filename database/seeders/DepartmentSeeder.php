<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Salary;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            [
                'name' => 'Business Development',
                'salary_id' => Salary::inRandomOrder()->first()->id
            ],
            [
                'name' => 'Create Services',
                'salary_id' => Salary::inRandomOrder()->first()->id
            ],
            [
                'name' => 'Engineering',
                'salary_id' => Salary::inRandomOrder()->first()->id
            ],
            [
                'name' => 'Legal',
                'salary_id' => Salary::inRandomOrder()->first()->id
            ],
            [
                'name' => 'Operations',
                'salary_id' => Salary::inRandomOrder()->first()->id
            ]
        ];

        Department::insert($departments);

    }
}
