<?php

namespace Tests\Feature;

use App\Models\Department;
use App\Models\Employee;
use App\Models\Salary;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SalaryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $salary = Salary::factory(1)->create([
            'amount' => 50
        ])->first();

        $department = Department::factory(1)->create([
            'name' => 'Legal',
            'salary_id' => $salary->id
        ])->first();

        $employee = Employee::factory(1)->create([
            'name' => 'Mahmoud',
            'email' => 'mahmoud@rms.com',
            'phone' => '01212479994',
        ])->first();

        $employee->departments()->attach([$department->id]);

        $this->assertEquals($employee->departments()->first()->salary->id, $salary->id);

    }
}
