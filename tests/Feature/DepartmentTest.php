<?php

namespace Tests\Feature;

use App\Models\Department;
use App\Models\Salary;
use Tests\TestCase;

class DepartmentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $salary = Salary::factory(1)->create([
            'amount' => 50
        ])->first();

        Department::factory(1)->create([
            'name' => 'Legal',
            'salary_id' => $salary->id
        ])->first();

        $this->assertDatabaseHas('departments', [
            'name' => 'Legal',
        ]);

    }
}
