<?php

namespace Tests\Feature;

use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        Employee::factory(1)->create([
            'name' => 'Mahmoud',
            'email' => 'mahmoud@rms.com',
            'phone' => '01212479994',
        ])->first();

        Employee::factory(1)->create([
            'name' => 'Mahmoud',
            'email' => 'mahmoud2@rms.com',
            'phone' => '01212479995',
        ])->first();

        $this->assertDatabaseCount('employees', 2);

    }
}
