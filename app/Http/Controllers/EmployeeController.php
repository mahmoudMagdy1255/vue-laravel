<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{

    public function index()
    {

        $employees = collect([]);

        if (\request('search') && is_numeric(request('search'))) {
            $employees = Employee::select('employees.*', DB::raw('departments.name as db_name'))
                ->join('department_employee', 'department_employee.employee_id', 'employees.id')
                ->join('departments', function ($query) {
                    $query->on('department_employee.department_id', 'departments.id');
                })->join(DB::raw('(SELECT id FROM `salaries` order by amount DESC limit '. request('search') . ',1) Salary'), function ($join) {
                    $join->on('departments.salary_id', 'Salary.id');
                })->simplePaginate();
        }
        return view('employee', compact('employees'));

    }
}
