<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Support\Facades\DB;

class AppController extends Controller
{
    public function form()
    {
        return view('form');
    }

    public function counter()
    {
        return view('counter');
    }
}
