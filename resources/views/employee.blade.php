@extends('layout')
@section('content')
    <div class="container mt-5">
        <form class="row">
            <div class="form-group col-8">
                <input class="form-control" name="search" type="number" min="1" max="50"
                       placeholder="Enter Number Between [1-50]" value="{{request('search')}}">
            </div>
            <div class="form-group col-4">
                <input class="form-control btn btn-info" type="submit" value="Search">
            </div>
        </form>
        @if($employees->count() and count($employees->items()))

            <div class="text-center m-5">
                All employees take the {{request('search')}} highest salary
            </div>

            <table class="table mb-5">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Department</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <th scope="row">{{$loop->index + 1}}</th>
                        <td>{{$employee->name}}</td>
                        <td>{{$employee->email}}</td>
                        <td>{{$employee->phone}}</td>
                        <td>{{$employee->db_name}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="text-center" style="margin: 10px">
                {{$employees->withQueryString()->links()}}
            </div>
        @elseif(request('search'))
            <div class="text-center" style="margin: 10px">
                There's no any employees has the {{request('search')}} highest salary
            </div>

        @endif

    </div>

@endsection
