<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>

    <link rel="stylesheet" href="{{asset("css/app.css")}}">
    <link rel="stylesheet" href="{{asset("css/demo.css")}}">
    @stack('css')

</head>

<body class="antialiased">

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarNav" style="    margin-left: 40%;">
        <ul class="navbar-nav">
            <li class="nav-item {{Route::current()->getName() == 'home' ? 'active' : ''}}">
                <a class="nav-link" href="{{url('/')}}">Home</a>
            </li>
            <li class="nav-item {{Route::current()->getName() == 'employees' ? 'active' : ''}}">
                <a class="nav-link" href="{{url('/employees')}}">Employees</a>
            </li>
            <li class="nav-item {{Route::current()->getName() == 'form' ? 'active' : ''}}">
                <a class="nav-link" href="{{url('/form')}}">Form</a>
            </li>
        </ul>
    </div>
</nav>

<div id="app">
    @yield('content')
</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="{{asset("js/app.js")}}"></script>
<script src="{{asset("js/kinetic.js")}}"></script>
<script src="{{asset("js/jquery.final-countdown.js")}}"></script>

@stack('script')

</body>


</html>
