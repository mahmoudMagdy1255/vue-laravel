@extends('layout')
@section('content')
    <div>
        <v-counter></v-counter>
    </div>

    @push('css')

        <style>
            body {
                background-color: rgba(44, 62, 80, 0.6);
                background-image: url({{asset('/img/bg.jfif')}});
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                font-family: 'Raleway', 'Arial', sans-serif;
            }

            html {
                background-image: url({{asset('/img/bg.jfif')}});
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

        </style>

    @endpush

    @push('script')

        <script type="text/javascript">

            $('document').ready(function () {
                counter();
            });

            $('#reset-count').on('click', function () {
                counter();
            })

            function counter() {
                var end = Math.floor((new Date("09/08/2022 01:40:25")).getTime() / 1000);
                var start = Math.floor((new Date("09/08/2022 00:00:00")).getTime() / 1000);
                var now = Math.floor((new Date("09/08/2022 00:00:00")).getTime() / 1000);
                $('.countdown').final_countdown({
                    'start': start,
                    'end': end,
                    'now': now
                });
            }

        </script>
    @endpush

@endsection
