// axios
import axios from 'axios'

axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('adminToken')}`;
axios.defaults.headers.common['Accept-Language'] = 'ar';

export default axios.create({
    baseURL:'https://staging.mazaady.com/api',
})
