require('./bootstrap');

import Vue from 'vue'
import axios from './axios.js'


import Form from "./views/Form";
import Counter from "./views/Counter";

Vue.component('v-form' , Form);
Vue.component('v-counter' , Counter);

Vue.use(axios);

new Vue({
}).$mount('#app')

